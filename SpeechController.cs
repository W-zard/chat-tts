﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;

namespace BotSpeaker
{
    public abstract class SpeechController
    {
        public delegate void UpdateEvent();

        public abstract event UpdateEvent Update;

        public delegate void EndEvent();

        public ChatSpeech Info;

        public string TextToRead;

        public abstract void Prepare(string str, int rate);

        public abstract event EndEvent End;

        public bool IsReady = false;
        public bool Sayd = false;
        public abstract void Say();
        public abstract string GetName();

        public override string ToString()
        {
            return GetName();
        }

        public abstract void Abort();
    }

    public class ChatSpeech
    {
        public ChatSpeech()
        {
            
        }
        public ChatSpeech(string type, string name)
        {
            Type = type;
            Name = name;
        }
        public string Type = "NULL";
        public string Name = "null";

        public override string ToString()
        {
            return Type;
        }
    }
}
