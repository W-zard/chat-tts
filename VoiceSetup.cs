﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BotSpeaker
{
    public partial class VoiceSetup : Form
    {
        public VoiceSetup()
        {
            InitializeComponent();
        }

        void UpdList()
        {
            listBox1.Items.Clear();

            listBox1.BeginUpdate();
            foreach (SpeechController speech in Configures.Main.Speeches)
            {
                listBox1.Items.Add(speech.GetName());
            }
            listBox1.EndUpdate();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            AddVoice av = new AddVoice();
            if (av.ShowDialog() == DialogResult.OK)
            {
                UpdList();
            }
        }

        private void VoiceSetup_Load(object sender, EventArgs e)
        {
            UpdList();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Configures.Main.Speeches.Remove(Configures.Main.Speeches[listBox1.SelectedIndex]);
        }
    }
}
