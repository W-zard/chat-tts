﻿using System.Windows.Forms;

namespace BotSpeaker
{
    public partial class Log : Form
    {
        public Log()
        {
            InitializeComponent();
            richTextBox1.Text = LogController.GetLog();
        }
    }
}