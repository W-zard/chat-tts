﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BotSpeaker.goodgame
{
    public class GoodGameMessage
    {
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        [JsonProperty(PropertyName = "data")]
        public GGData Data { get; set; }

    }

    public class GGData
    {
        [JsonProperty(PropertyName = "channel_id")]
        public int ChannelID { get; set; }
        
        [JsonProperty(PropertyName = "user_name")]
        public string UserName { get; set; }

        [JsonProperty(PropertyName = "amount")]
        public string Amount { set; get; }

        [JsonProperty(PropertyName = "message")]
        public string Message { set; get; }

        [JsonProperty(PropertyName = "text")]
        public string Text { get; set; }
    }
}
