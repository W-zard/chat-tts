﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using SimpleJson;
using WebSocket4Net;
using xNet;
using ErrorEventArgs = SuperSocket.ClientEngine.ErrorEventArgs;

namespace BotSpeaker.goodgame
{
    public class Chat : ChatController
    {
        public override event UpdateEvent Update;
        public override event DonateEvent Donate;
        public override event ErrorEvent Error;

        private string Token;
        private string UserId;
        
        private WebSocket ws;

        public Chat(ChatChannel ch)
        {
            Info = ch;

            WebClient wc = new WebClient();

            Regex r = new Regex("channelId:[ ]*'([0-9]*)',");
            MatchCollection m = r.Matches(wc.DownloadString("http://goodgame.ru/chat/" + ch.ChannelUser));

            ChatPath = m[0].Groups[1].Value;

            if (ch.NeedAuth)
            {
                GetAuthData();
            }

            ChatName = "GoodGame-" + ChatPath;

            StreamerName = ch.ChannelUser;

            InitWS();
        }
        
        private void InitWS()
        {
            ws = new WebSocket("ws://chat.goodgame.ru:8081/chat/websocket", "", WebSocketVersion.Rfc6455);

            ws.MessageReceived += WS_Incoming;
            ws.Error += WS_Err;
            ws.Closed += WS_Closed;
            ws.Open();
        }

        private void WS_Closed(object sender, EventArgs e)
        {
            InitWS();
        }

        private void WS_Err(object sender, ErrorEventArgs e)
        {
            if (ws.State == WebSocketState.Open)
            {
                ws.Close();
            }
        }

        private bool ath = false;

        private void WS_Incoming(object sender, MessageReceivedEventArgs e)
        {
            GoodGameMessage msg = JsonConvert.DeserializeObject<GoodGameMessage>(e.Message);

            switch (msg.Type)
            {
                case "success_auth":
                {
                    if (msg.Data.UserName != "" && !ath)
                    {
                        ath = true;
                        ChatName += "-" + msg.Data.UserName;
                    }
                    break;
                }
                case "welcome":
                    Connect();
                    break;
                case "message":
                    GetChat(msg.Data.UserName, msg.Data.Text);
                    break;
                case "payment":
                    if (Donate != null)
                        Donate(new DonateMessage(msg.Data.UserName, msg.Type, msg.Data.Amount, msg.Data.Message));
                    break;
                case "premium":
                    if (Donate != null)
                        Donate(new DonateMessage(msg.Data.UserName, msg.Type, "", ""));
                    break;
            }
        }

        private void Connect()
        {
            string s = @"{'type':'auth','data':{'user_id':'".Replace("'", "\"") + UserId + "','token':'".Replace("'", "\"") + Token + "'}}".Replace("'", "\"");
            if(Info.NeedAuth) ws.Send(s);

            s = "{'type': 'join', 'data': {'channel_id': ".Replace("'", "\"") + ChatPath +", 'hidden': false}}".Replace("'", "\"");
            ws.Send(s);
        }

        private void GetChat(string name, string text)
        {
            if (Closing) return;

            if (!Configures.Main.ActiveNames.Contains(name)) Configures.Main.ActiveNames.Add(name);

            Update?.Invoke(GetMessage(name,text));

            LogController.Add(ChatName + "-> " + name + ": " + text);

        }

        public ChatMessage GetMessage(string name, string text)
        {
            Regex rgx = new Regex("<a .*?</a>", RegexOptions.IgnoreCase);
            MatchCollection ms = rgx.Matches(text);

            string txt = ms.Cast<Match>().Aggregate(text, (current, m) => current.Replace(m.Value, " Link "));

            txt = CleanUp(txt);

            List<string> tonames = new List<string>();
            
            rgx = new Regex("\\w+");
            ms = rgx.Matches(txt);

            bool ToStreamer = false;

            List<string> to = new List<string>();

            foreach (Match m in ms)
            {
                ToStreamer = m.Value == StreamerName;

                if (Configures.Main.ActiveNames.Contains(m.Value))
                {
                    to.Add(m.Value);
                    
                    txt = txt.Replace(m.Value, " ");
                }
            }
            
            return new ChatMessage(name,to.ToArray(),txt,ToStreamer);
        }

        public string Passwd = SAPI.GGPasswd;
        
        public void GetAuthData()
        {
            HttpRequest token = new HttpRequest();
            MultipartContent c = new MultipartContent();
            c.Add(new StringContent(StreamerName), "login");
            c.Add(new StringContent(Passwd), "password");
            string s = token.Post("http://goodgame.ru/ajax/chatlogin/", c).ToString();

            Regex rgx = new Regex("\"token\":\"(.*?)\",");
            Token = rgx.Match(s).Groups[1].Value;
            rgx = new Regex("\"token\":\"(.*?)\",");
            UserId = rgx.Match(s).Groups[1].Value;
        }

        public override void SendMessage(ChatMessage msg)
        {
            if(!ath) return;

            string s = @"{'type':'send_message','data':{'channel_id':'"+ChatPath+ "','text':'" + msg.GoodGame() + "','token':'".Replace("'", "\"") + Token + "','hideIcon':'" + "false" + "','mobile':'" + "false" + "'}}";
            ws.Send(s.Replace("'", "\""));
        }
    }
}