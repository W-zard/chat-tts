﻿using System;
using System.IO;
using System.Windows.Forms;

namespace BotSpeaker
{
    internal static class Program
    {
        public static string SavePath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData) + "\\BotSpeech\\"; //"BotSpeech\\";  // System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData) + "\\BotSpeech\\";

        /// <summary>
        ///     Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            if (!Directory.Exists(SavePath)) Directory.CreateDirectory(SavePath);

            Configures.Main = new MainConfig();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}