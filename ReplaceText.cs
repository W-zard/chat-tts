﻿using System;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using SpeechLib;

namespace BotSpeaker
{
    public partial class ReplaceText : Form
    {
        private ChatText t;

        public ReplaceText(int s)
        {
            InitializeComponent();

            t = s != -1 ? Configures.Main.TextReplace[s] : new ChatText();

            textBox1.Text = t.Original;

            foreach (SpeechPref chatSpeech in Configures.ReadingOptions)
            {
                comboBox1.Items.Add(chatSpeech.RuleName);
            }
            comboBox1.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(textBox1.Text.Replace(" ","") != "")
                t.Original = textBox1.Text.ToLower().Trim();
            
            if (!Configures.Main.TextReplace.Contains(t))
                Configures.Main.TextReplace.Add(t);

            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (Configures.Main.TextReplace.Contains(t))
                Configures.Main.TextReplace.Remove(t);

            Close();
        }
        
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (ChatSpeech chatSpeech in t.Replace)
            {
                if (chatSpeech.Type == comboBox1.SelectedItem.ToString())
                {
                    textBox2.Text = chatSpeech.Name;
                    return;
                }
            }
            textBox2.Text = "";
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            foreach (ChatSpeech chatSpeech in t.Replace)
            {
                if (chatSpeech.Type == comboBox1.SelectedItem.ToString())
                {
                    chatSpeech.Name = textBox2.Text;
                    return;
                }
            }
            t.Replace.Add(new ChatSpeech(comboBox1.SelectedItem.ToString(), textBox2.Text));
        }
    }
}