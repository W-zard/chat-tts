﻿using System;
using System.Linq;
using System.Windows.Forms;
using SpeechLib;

namespace BotSpeaker
{
    public partial class NameReplase : Form
    {
        private ChatName ct;

        public NameReplase(string name)
        {
            InitializeComponent();

            textBox1.Text = name;

            foreach (SpeechPref chatSpeech in Configures.ReadingOptions)
            {
                comboBox1.Items.Add(chatSpeech.RuleName);
            }

            if (Configures.Main.NickNames.ContainsKey(textBox1.Text)) ct = Configures.Main.NickNames[name];
            else
            {
                ct = new ChatName(name,"");
                Configures.Main.NickNames.Add(name, ct);
            }

            comboBox1.SelectedIndex = 0;
        }

        
        private void button2_Click(object sender, EventArgs e)
        {
            
            Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if ( Configures.Main.NickNames.ContainsKey(textBox1.Text))
                 Configures.Main.NickNames.Remove(textBox1.Text);
            
            Close();
        }

        private void NameReplase_Load(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (ChatSpeech speech in ct.Replace)
            {
                if (speech.Type == comboBox1.SelectedItem.ToString())
                {
                    textBox2.Text = speech.Name;
                    return;
                }
            }
            textBox2.Text = "";
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < ct.Replace.Count; i++)
                {
                    ChatSpeech speech = ct.Replace[i];
                    if (speech.Type == comboBox1.SelectedItem.ToString())
                    {
                        speech.Name = textBox2.Text;
                        return;
                    }
                }
                ct.Replace.Add(new ChatSpeech(comboBox1.SelectedItem.ToString(),textBox2.Text));
            
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            ct.Ignore = checkBox1.Checked;
        }
    }
}