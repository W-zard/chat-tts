﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using IrcDotNet;

namespace BotSpeaker.twitch
{
    internal class Chat : ChatController
    {
        public override event UpdateEvent Update;
        public override event DonateEvent Donate;
        public override event ErrorEvent Error;

        private string host = "irc.twitch.tv";
        private IrcClient Irc;
        private int port = 6667;

        public Chat(string id)
        {
            ChatPath = id;
            ChatName = "Twitch-" + id;

            StreamerName = id;

            Init();
        }

        public Chat(ChatChannel ch)
        {
            Info = ch;

            ChatPath = ch.ChannelUser;
            ChatName = "Twitch-" + ch.ChannelUser;

            StreamerName = ch.ChannelUser;

            Init();
        }

        private void Init()
        {
            if (Closing) return;

            if (Irc != null)
            {
                Irc.Disconnect();
                Irc = null;
            }

            Irc = new IrcClient();

            Irc.RawMessageReceived += IncomeMessage;
            Irc.Connected += Connecting;

            Irc.Error += Err;
            Irc.ProtocolError += PErr;
            Irc.Disconnected += Disc;
            Irc.ConnectFailed += CFail;

            Random r = new Random();
            string s = "justinfan" + r.Next(100000000);

            IrcUserRegistrationInfo i = new IrcUserRegistrationInfo();
            i.NickName = s;
            i.UserName = s;
            i.RealName = s;
            i.Password = s;

            Irc.Connect("irc.twitch.tv", 6667, false, i);
        }

        private void CFail(object sender, IrcErrorEventArgs e)
        {
            Init();
        }

        private void Disc(object sender, EventArgs e)
        {
            Init();
        }

        private void PErr(object sender, IrcProtocolErrorEventArgs e)
        {
            Init();
        }

        private void Err(object sender, IrcErrorEventArgs e)
        {
            Init();
        }

        private void Connecting(object sender, EventArgs e)
        {
            Irc.Channels.Join("#" + ChatPath);
        }

        private void IncomeMessage(object sender, IrcRawMessageEventArgs e)
        {
            if (Closing) return;

            try
            {
                Regex r = new Regex(":(.*)!\\1@\\1.tmi.twitch.tv PRIVMSG #\\w+ :(.*)");
                Match m = r.Match(e.RawContent);
                if (m.Groups[1].Value != "")
                    GetChat(m.Groups[1].Value, m.Groups[2].Value);
            }
            catch {}
        }

        private void GetChat(string name, string text)
        {
            if (!Configures.Main.ActiveNames.Contains(name)) Configures.Main.ActiveNames.Add(name);


            Update?.Invoke(GetMessage(name,text));
            //Form1.voice.Speak(tospeach(name, text),
            //                  SpeechVoiceSpeakFlags.SVSFNLPSpeakPunc | SpeechVoiceSpeakFlags.SVSFlagsAsync);

            LogController.Add(ChatName + "-> " + name + ": " + text);

        }

        public ChatMessage GetMessage(string name, string text)
        {
            bool ToStreamer = false;

            Regex rgx = new Regex("<a .*?</a>", RegexOptions.IgnoreCase);
            MatchCollection ms = rgx.Matches(text);

            string txt = ms.Cast<Match>().Aggregate(text, (current, m) => current.Replace(m.Value, " Link "));

            txt = CleanUp(txt);
            
            List<string> to = new List<string>();

            rgx = new Regex("@(\\w+)");
            ms = rgx.Matches(txt);
            foreach (Match m in ms)
            {
                ToStreamer = m.Groups[1].Value == StreamerName;
                
                if(!to.Contains(m.Groups[1].Value)) to.Add(m.Groups[1].Value);

                txt = txt.Replace(m.Groups[1].Value, "");
            }

            return new ChatMessage(name, to.ToArray(), txt, ToStreamer);
        }

        public override void SendMessage(ChatMessage msg)
        {
            
        }

        public new void Dispose()
        {
            Closing = true;

            Irc.Disconnect();
            Irc = null;

            GC.SuppressFinalize(this);
        }
    }
}