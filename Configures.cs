﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using BotSpeaker.Speech;

namespace BotSpeaker
{
    public static class Configures
    {
        public static void Init()
        {
            Main = new MainConfig();
            Main.Load();

            ReaderConfig = DataLoad.Load<ChatConfigs>("config");

            if (File.Exists(Program.SavePath + "tags")) TagList = DataLoad.Load<List<Tag>>("tags");
            else
            {
                TagList = new List<Tag>();
                TagList.Add(new Tag("Обычное сообщение", "%NameFrom говорит, "));
                TagList.Add(new Tag("Сообщение пользователю", "%NameFrom говорит юзеру %NameTo, "));
                TagList.Add(new Tag("Сообщение пользователям", "%Name говорит говорит группе юзеров, "));
                TagList.Add(new Tag("Сообщение стриммеру", "%Name говорит стриммеру, "));
            }

            if (File.Exists(Program.SavePath + "pref")) ReadingOptions = DataLoad.Load<List<SpeechPref>>("pref");
            else
            {
                ReadingOptions = new List<SpeechPref>();
                ReadingOptions.Add(new SpeechPref("[Default]","none","Стандарное правило выбора голоса",-1));
            }
        }

        public static void SaveAll()
        {
            SaveReaderConfig();
            SaveTags();
            SaveReadingOptions();
            SaveNames();
            Savetext();
            SaveChatChannel();
            SaveChatSpeech();
        }

        public static void SaveReaderConfig()
        {
            DataLoad.Save(ReaderConfig, "config");
        }
        public static void SaveTags()
        {
            DataLoad.Save(TagList, "tags");
        }
        public static void SaveReadingOptions()
        {
            DataLoad.Save(ReadingOptions, "pref");
        }
        public static void SaveNames()
        {
            List<ChatName> tmp = new List<ChatName>();

            foreach (KeyValuePair<string, ChatName> pair in Main.NickNames)
            {
                tmp.Add(pair.Value);
            }
            
            DataLoad.Save(tmp, "names");
        }
        public static void Savetext()
        {
            DataLoad.Save(Main.TextReplace, "text");
        }
        public static void SaveChatChannel()
        {
            List<ChatChannel> tmp1 = new List<ChatChannel>();

            foreach (ChatController chat in Main.Chats)
            {
                tmp1.Add(chat.Info);
            }
            
            DataLoad.Save(tmp1, "connections");
        }
        public static void SaveChatSpeech()
        {
            List<ChatSpeech> tmp2 = new List<ChatSpeech>();
            foreach (SpeechController speech in Main.Speeches)
            {
                tmp2.Add(speech.Info);
            }
            DataLoad.Save(tmp2, "spechengines");
        }

        public static MainConfig Main;
        public static ChatConfigs ReaderConfig;
        public static List<Tag> TagList;
        public static List<SpeechPref> ReadingOptions;
    }

    public class MainConfig
    {
        public MainConfig()
        {
            
        }

        public void Load()
        {

            #region names
            List<ChatName> tmp = DataLoad.Load<List<ChatName>>("names");
            foreach (ChatName name in tmp)
            {
                NickNames.Add(name.Original, name);
            }
            tmp.Clear();
            #endregion

            #region text
            TextReplace = DataLoad.Load<List<ChatText>>("text");
            #endregion

            #region channels
            List<ChatChannel> tmp1 = DataLoad.Load<List<ChatChannel>>("connections");
            foreach (ChatChannel channel in tmp1)
            {
                Channels.CreateChannel(channel);
            }
            #endregion

            #region speechs
            List<ChatSpeech> tmp2 = DataLoad.Load<List<ChatSpeech>>("spechengines");
            foreach (ChatSpeech speech in tmp2)
            {
                switch (speech.Type)
                {
                    case "SAPI":
                        {
                            Speeches.Add(new SAPI(speech.Name));
                            break;
                        }
                    case "Google":
                        {
                            Speeches.Add(new WebGoogle(speech.Name));
                            break;
                        }
                }
            }
            #endregion

        }

        public Dictionary<string, ChatName> NickNames = new Dictionary<string, ChatName>();
        public List<ChatController> Chats = new List<ChatController>();
        public List<ChatText> TextReplace = new List<ChatText>();

        public List<string> ActiveNames = new List<string>();

        public List<SpeechController> Speeches = new List<SpeechController>(); 
    }

    public class ChatConfigs
    {
        public bool OnlyToStreamer;
        public bool Ignore;

        public string[] IgnoreStrings = new string[0];

        public int WordMaximum;
        public int SpeechMaximum;
    }

    public class SpeechPref
    {
        public SpeechPref(string ruleName, string speechName, string ruleOps, int ruleType)
        {
            RuleName = ruleName;
            SpeechName = speechName;
            RuleOps = ruleOps;
            RuleType = ruleType;
        }

        public SpeechPref()
        {
            RuleName = "";
            SpeechName = "";
            RuleOps = "";
            RuleType = 0;
        }

        public string RuleName;
        public string SpeechName;
        public string RuleOps;
        public int RuleType;
    }

    #region Structs
    public class ChatName
    {
        public bool Ignore;

        public string IndividualSpeech;

        public string Original;
        public List<ChatSpeech> Replace = new List<ChatSpeech>();
        public ChatName() { }

        public ChatName(string name, string replace)
        {
            Original = name;
            Replace.Add(new ChatSpeech("[Default]", replace));
        }
    }
    public class ChatText
    {
        public bool Ignore;
        public string Original;
        public List<ChatSpeech> Replace = new List<ChatSpeech>();
    }

    public class ChatChannel
    {
        public ChatChannel()
        {
            
        }

        public ChatChannel(string server, string channelUser, string channelPasswd, string channelHost)
        {
            Server = server;
            ChannelUser = channelUser;
            ChannelPasswd = channelPasswd;
            ChannelHost = channelHost;
        }
        public string Server;
        public string ChannelUser;

        public bool NeedAuth = false;

        private string passwd;

        public string ChannelPasswd
        {
            set
            {
                NeedAuth = true;
                passwd = value;
            }
            get { return passwd; }

        }
        public string ChannelHost;
    }

    public static class DataLoad
    {
        public static void Save<T>(T data, string path)
        {
            StreamWriter sw = new StreamWriter(Program.SavePath + path);
            
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            serializer.Serialize(sw, data);

            sw.Close();
        }

        public static T Load<T>(string path) where T : new()
        {
            if (!File.Exists(Program.SavePath + path)) return new T();
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            StreamReader sw = new StreamReader(Program.SavePath + path);

            T list = (T)serializer.Deserialize(sw);
            sw.Close();
            return list;
        }
    }
    #endregion

}
