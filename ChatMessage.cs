﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BotSpeaker
{
    public class ChatMessage
    {
        public ChatMessage(string from, string text)
        {
            From = from;
            Text = text;
        }

        public ChatMessage(string from, string to, string text, bool isPrivate = false)
        {
            IsPrivate = isPrivate;
            From = @from;
            To = new []{to};
            Text = text;
        }

        public ChatMessage(string from, string to, string text, bool toStreammer, bool isPrivate = false)
        {
            IsPrivate = isPrivate;
            From = from;
            To = new []{to};
            Text = text;
            ToStreammer = toStreammer;
        }

        public ChatMessage(string from, string[] to, string text, bool toStreammer)
        {
            From = from;
            To = to;
            Text = text;
            ToStreammer = toStreammer;
        }

        public string From;
        public string[] To;

        public string Text;

        public bool IsPrivate;

        public bool ToStreammer=false;

        public string GoodGame()
        {
            string text = "";

            foreach (string s in To)
            {
                text += (IsPrivate? "@" : "") + s + ",";
            }

            text += WebUtility.HtmlEncode(Text);

            return text;
        }

    }

    public class DonateMessage
    {
        public string UserName;
        public string DonateType;
        public string DonateValue;
        public string DonateText;

        public DonateMessage(string userName, string donateType, string donateValue, string donateText)
        {
            UserName = userName;
            DonateType = donateType;
            DonateValue = donateValue;
            DonateText = donateText;
        }
    }
}
