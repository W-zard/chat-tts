﻿using System;
using System.Windows.Forms;

namespace BotSpeaker
{
    public partial class TextReplase : Form
    {
        public TextReplase()
        {
            InitializeComponent();
            ListUpd();
        }

        private void ListUpd()
        {
            listBox1.Items.Clear();
            foreach (ChatText pair in Configures.Main.TextReplace)
            {
                listBox1.Items.Add(pair.Original);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ReplaceText rt = new ReplaceText(-1);
            rt.ShowDialog();
            ListUpd();
        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == -1) return;
            ReplaceText rt = new ReplaceText(listBox1.SelectedIndex);
            rt.ShowDialog();
            ListUpd();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == -1 || listBox1.SelectedIndex == 0) return;

            ChatText a = Configures.Main.TextReplace[listBox1.SelectedIndex];
            ChatText b = Configures.Main.TextReplace[listBox1.SelectedIndex - 1];

            int z = listBox1.SelectedIndex;

            Configures.Main.TextReplace[listBox1.SelectedIndex] = b;
            Configures.Main.TextReplace[listBox1.SelectedIndex - 1] = a;

            ListUpd();
            listBox1.SelectedIndex = z - 1;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == -1 || listBox1.SelectedIndex == listBox1.Items.Count - 1) return;

            ChatText a = Configures.Main.TextReplace[listBox1.SelectedIndex];
            ChatText b = Configures.Main.TextReplace[listBox1.SelectedIndex + 1];

            int z = listBox1.SelectedIndex;

            Configures.Main.TextReplace[listBox1.SelectedIndex] = b;
            Configures.Main.TextReplace[listBox1.SelectedIndex + 1] = a;

            ListUpd();
            listBox1.SelectedIndex = z + 1;
        }

        private void TextReplase_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }
    }
}