﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BotSpeaker
{
    public partial class Tags : Form
    {
        public Tags()
        {
            InitializeComponent();
            ListUpd();
        }

        void ListUpd()
        {
            listBox1.Items.Clear();
            foreach (Tag tag in Configures.TagList)
            {
                listBox1.Items.Add(tag.TagCode);
            }
        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                TagEditor te = new TagEditor(Configures.TagList[listBox1.SelectedIndex], listBox1.SelectedIndex <=3);
                if (te.ShowDialog() == DialogResult.OK)
                {
                    Configures.TagList.Add(te.Result);
                }
            }
            ListUpd();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            TagEditor te = new TagEditor(new Tag(), true);
            if (te.ShowDialog() == DialogResult.OK)
            {
                Configures.TagList.Add(te.Result);
            }
            ListUpd();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if(listBox1.SelectedIndex<=3) return;

            Configures.TagList.Remove(Configures.TagList[listBox1.SelectedIndex]);
            ListUpd();
        }
    }
}
