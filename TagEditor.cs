﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BotSpeaker
{
    public partial class TagEditor : Form
    {

        public Tag Result;

        public TagEditor(Tag selectedTag, bool locked)
        {
            InitializeComponent();

            textBox1.ReadOnly = locked;

            Result = selectedTag;
            textBox1.Text = Result.TagCode;
            
            foreach (SpeechPref chatSpeech in Configures.ReadingOptions)
            {
                comboBox1.Items.Add(chatSpeech.RuleName);
            }
            comboBox1.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Replace(" ", "").Length <= 1) goto End;
            if (richTextBox1.Text.Replace(" ", "").Length <= 1) goto End;

            if (!textBox1.ReadOnly) Result.TagCode = textBox1.Text;

            DialogResult = DialogResult.OK;
            Close();
            End:
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (ChatSpeech chatSpeech in Result.TagText)
            {
                if (chatSpeech.Type == comboBox1.SelectedItem.ToString())
                {
                    richTextBox1.Text = chatSpeech.Name;
                    return;
                }
            }
            richTextBox1.Text = "";
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            foreach (ChatSpeech chatSpeech in Result.TagText)
            {
                if (chatSpeech.Type == comboBox1.SelectedItem.ToString())
                {
                    chatSpeech.Name = richTextBox1.Text;
                    return;
                }
            }
            Result.TagText.Add(new ChatSpeech(comboBox1.SelectedItem.ToString(),richTextBox1.Text));
        }
    }
}
