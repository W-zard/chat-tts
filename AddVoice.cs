﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using BotSpeaker.Speech;
using SpeechLib;

namespace BotSpeaker
{
    public partial class AddVoice : Form
    {
        public AddVoice()
        {
            InitializeComponent();

            comboBox1.SelectedIndex = 0;
        }

        private void AddVoice_Load(object sender, EventArgs e)
        {
            SpVoice voice = new SpVoice();
            foreach (SpObjectToken token in voice.GetVoices("", ""))
            {
                comboBox2.Items.Add(token.GetDescription(0));
            }
            comboBox2.SelectedIndex = 0;
        }

        private void buttonC_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        SpeechController sc = null;

        private void buttonT_Click(object sender, EventArgs e)
        {
            
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                {
                    sc = new SAPI(comboBox2.SelectedItem.ToString());
                    break;
                }
                case 1:
                {
                    sc = new WebGoogle(textBox1.Text);
                    break;
                }
            }
            sc.End += () =>
                      { MessageBox.Show("Проверка завершена"); };
            sc.Prepare("Если вы слышите это, голосовой синтезатор работает!",0);
            while (!sc.IsReady)
            {
                Thread.Sleep(10);
            }
            sc.Say();
        }

        private void buttonS_Click(object sender, EventArgs e)
        {
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    {
                        sc = new SAPI(comboBox2.SelectedItem.ToString());
                        break;
                    }
                case 1:
                    {
                        sc = new WebGoogle(textBox1.Text);
                        break;
                    }
            }

            Configures.Main.Speeches.Add(sc);

            DialogResult = DialogResult.OK;
            Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            panelSAPI.Visible = comboBox1.SelectedIndex == 0;
            panelGoogle.Visible = comboBox1.SelectedIndex == 1;
        }
    }
}
