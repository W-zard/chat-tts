﻿using System;
using System.Windows.Forms;

namespace BotSpeaker
{
    public partial class Channels : Form
    {
        public Channels()
        {
            InitializeComponent();

            UpdList();
        }

        private void UpdList()
        {
            listBox1.Items.Clear();
            for (int i = 0; i < Configures.Main.Chats.Count;)
            {
                ChatController s = Configures.Main.Chats[i];
                if (s.ChatName == null)
                {
                    Configures.Main.Chats.Remove(s);
                    continue;
                }
                listBox1.Items.Add(s.ChatName);
                i++;
            }
        }

        private void добавитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddChannel ac = new AddChannel(new ChatChannel());

            if (ac.ShowDialog() != DialogResult.None)
            {
                CreateChannel(ac.Result);
                UpdList();
            }
        }

        public static void CreateChannel(ChatChannel cc)
        { 
            if(cc==null)return;
            switch (cc.Server.ToLower())
            {
                case "sc2tv":
                {
                    Configures.Main.Chats.Add(new sc2tv.Chat(cc));
                    break;
                }
                case "goodgame":
                {
                    Configures.Main.Chats.Add(new goodgame.Chat(cc));
                    break;
                }
                case "twitch":
                {
                    Configures.Main.Chats.Add(new twitch.Chat(cc));
                    break;
                }
            }
        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == -1) return;
            Configures.Main.Chats[listBox1.SelectedIndex].Dispose();
            Configures.Main.Chats.Remove(Configures.Main.Chats[listBox1.SelectedIndex]);
            UpdList();
        }
    }
}