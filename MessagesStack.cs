﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BotSpeaker
{
    public static class MessagesStack
    {
        public static List<SpeechController> Stack = new List<SpeechController>();

        public static bool ReadingNow = false;

        public static Thread t;

        public static void Read()
        {
            MessagesStack.t = new Thread(() =>
            {
                MessagesStack.ReadingNow = true;
                if (MessagesStack.Stack.Count == 0)
                {
                    MessagesStack.ReadingNow = false;
                    return;
                }

                SpeechController tmp = MessagesStack.Stack[0];
                tmp.End += Read;
                while (!tmp.IsReady)
                {
                    Thread.Sleep(10);
                }
                tmp.Say();
                MessagesStack.Stack.Remove(tmp);
            });
            MessagesStack.t.Start();
        }
    }
}
