﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using Newtonsoft.Json;
using SpeechLib;

namespace BotSpeaker.sc2tv
{
    public class Chat : ChatController
    {
        public override event UpdateEvent Update;
        public override event DonateEvent Donate;
        public override event ErrorEvent Error;

        private string strID = "";

        private Messages chat = new Messages();
        private readonly WebClient wc = new WebClient();

        public Chat(ChatChannel ch)
        {
            Info = ch;

            Regex rgx = new Regex(@"current_channel_id = ([0-9]*);", RegexOptions.IgnoreCase);

            try
            {
                strID = rgx.Matches(wc.DownloadString("http://sc2tv.ru/channel/" + ch.ChannelUser))[0].Groups[0].Value;
            }
            catch (Exception e)
            {
                MessageBox.Show("Чегот не то", e.Message);
                return;
            }

            if (strID == "")
            {
                MessageBox.Show("Чегот не то", "Нет совпадений");
                return;
            }

            ChatName = "SC2TV-" + strID;

            ChatPath = "http://chat.sc2tv.ru/memfs/channel-" + strID + ".json";

            thread = new Thread(e =>
            {
                while (true)
                {



                    GetChat(GetChatText());
                    Thread.Sleep(750);
                }
            });
            thread.Start();
        }

        private void GetChat(string s)
        {
            if (Closing) return;

            chat = JsonConvert.DeserializeObject<Messages>(s);

            if (chat == null) return;

            if (LastId == -1) LastId = chat.Content[0].Id;

            Stack<Message> stack = new Stack<Message>();

            foreach (Message message in chat.Content)
            {
                if (!Configures.Main.ActiveNames.Contains(message.Name)) Configures.Main.ActiveNames.Add(message.Name);

                if (message.Name == "SC2TV") continue;
                
                if (message.Id == LastId) break;

                stack.Push(message);
            }

            foreach (Message message in stack)
            {
                Update?.Invoke(GetMessage(message));

                LogController.Add(ChatName + "-> [" + message.Date.Day.ToString("00") + "/" +
                                  message.Date.Month.ToString("00") + " " + message.Date.Hour + ":" +
                                  message.Date.Minute.ToString("00") + ":" + message.Date.Second.ToString("00") + "] " +
                                  message.Name + ": " + message.Text);
            }

            LastId = chat.Content[0].Id;

        }

        public ChatMessage GetMessage(Message msg)
        {
            bool ToStreamer = false;

            Regex rgx = new Regex(@"\[url.*?\[/url\]", RegexOptions.IgnoreCase);
            MatchCollection ms = rgx.Matches(msg.Text);

            string txt = ms.Cast<Match>().Aggregate(msg.Text, (current, m) => current.Replace(m.Value, " Link "));

            txt = CleanUp(txt);

            rgx = new Regex(":(.*?):");
            ms = rgx.Matches(txt);
            
            rgx = new Regex("\\[b\\](.*?)\\[/b\\]");
            ms = rgx.Matches(txt);

            List<string> to = new List<string>();

            foreach (Match m in ms)
            {
                ToStreamer = m.Groups[1].Value == StreamerName;

                if(!to.Contains(m.Groups[1].Value)) to.Add(m.Groups[1].Value);

                txt = txt.Replace(m.Value, " ");
            }

            return new ChatMessage(msg.Name,to.ToArray(), txt, ToStreamer);
        }

        private string GetChatText()
        {
            S:
            try
            {
                WebRequest request = WebRequest.Create(ChatPath);
                request.Timeout = 1500;
                WebResponse response = request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                return reader.ReadToEnd();
            }
            catch
            {
                goto S;
            }
        }

        public override void SendMessage(ChatMessage msg)
        {
            
        }
    }
}