﻿using System;
using Newtonsoft.Json;

namespace BotSpeaker
{
    internal class Messages
    {
        [JsonProperty(PropertyName = "messages")]
        internal Message[] Content { get; set; }
    }

    public class Message
    {
        [JsonProperty(PropertyName = "channelId")]
        public int ChannelId { get; set; }

        [JsonProperty(PropertyName = "date")]
        public DateTime Date { get; set; }

        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "uid")]
        public int Uid { get; set; }

        [JsonProperty(PropertyName = "message")]
        public string Text { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        public int NeedToDelete { get; set; }

        public override string ToString()
        {
            return Text;
        }
    }
}