﻿using System;
using System.Linq;
using System.Net;
using System.Threading;

namespace BotSpeaker
{
    public abstract class ChatController
    {
        public delegate void UpdateEvent(ChatMessage msg);
        public abstract event UpdateEvent Update;
        public delegate void DonateEvent(DonateMessage msg);
        public abstract event DonateEvent Donate;
        public delegate void ErrorEvent(string msg);
        public abstract event ErrorEvent Error;



        public string ChatName;
        public string ChatPath;
        public bool Closing;
        public int LastId = -1;
        public string StreamerName;
        public Thread thread;
        
        public ChatChannel Info;

        ~ChatController()
        {
            try
            {
                thread.Abort();
            }
            catch {}
        }

        public void Dispose()
        {
            Closing = true;
            try
            {
                thread.Abort();
            }
            catch {}
            GC.SuppressFinalize(this);
        }

        public string CleanUp(string s)
        {
            return WebUtility.HtmlDecode(s).Replace(":free-", ":").
                Replace(":s:", ":").
                Replace("\"", " . ");
        }

        public abstract void SendMessage(ChatMessage msg);
    }
}