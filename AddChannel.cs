﻿using System;
using System.Windows.Forms;

namespace BotSpeaker
{
    public partial class AddChannel : Form
    {
        public ChatChannel Result;

        public AddChannel(ChatChannel ch)
        {
            Result = ch;
            InitializeComponent();
            if (ch.Server != "")
                comboBox1.Text = Result.Server;
            else comboBox1.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Result.Server = comboBox1.Text;

            Result.ChannelID = textBox1.Text;
            Result.ChannelHost = textBox2.Text;

            DialogResult = DialogResult.OK;
            Close();
        }
    }
}