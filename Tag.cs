﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BotSpeaker
{
    public class Tag
    {
        public Tag()
        {
            
        }
        public Tag(string tagCode, string tagText)
        {
            TagCode = tagCode;
            TagText.Add(new ChatSpeech("[Default]", tagText));
        }

        public List<ChatSpeech> TagText = new List<ChatSpeech>(); 

        public string TagCode;

        public string CompileString(ChatMessage input)
        {
            return "";
        }
    }
}
