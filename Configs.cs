﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BotSpeaker
{
    public partial class Configs : Form
    {
        public Configs()
        {
            InitializeComponent();
            foreach (string ignoreString in Configures.ReaderConfig.IgnoreStrings)
            {
                richTextBox1.Text += ignoreString + "\n";
            }
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            Configures.ReaderConfig.WordMaximum = (int)numericUpDown1.Value;
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            Configures.ReaderConfig.SpeechMaximum = (int)numericUpDown2.Value;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            Configures.ReaderConfig.OnlyToStreamer = checkBox1.Checked;
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            Configures.ReaderConfig.OnlyToStreamer = checkBox1.Checked;
        }

        private void Configs_FormClosing(object sender, FormClosingEventArgs e)
        {
            Configures.ReaderConfig.IgnoreStrings = richTextBox1.Text.Split(new[] {'\n'},
                                                                            StringSplitOptions.RemoveEmptyEntries);
        }
    }
}
