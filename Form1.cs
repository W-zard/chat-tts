﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Speech.Synthesis;
using System.Threading;
using System.Windows.Forms;
using BotSpeaker.Rule;
using BotSpeaker.sc2tv;
using BotSpeaker.Speech;
using SpeechLib;

namespace BotSpeaker
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            CheckForIllegalCrossThreadCalls = false;
        }

        private void UpdUserList()
        {
            if (listBox1.Items.Count != Configures.Main.ActiveNames.Count)
            {
                for (int i = listBox1.Items.Count; i < Configures.Main.ActiveNames.Count; i++)
                {
                    listBox1.Items.Add(Configures.Main.ActiveNames[i]);
                }
            }
        }
        
        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == -1) return;

            NameReplase nr = new NameReplase(listBox1.Items[listBox1.SelectedIndex].ToString());
            nr.ShowDialog();

            Configures.SaveNames();
        }
        
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Configures.SaveAll();

            LogController.sw.Close();

            foreach (ChatController chatController in Configures.Main.Chats)
            {
                chatController.Dispose();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            TextReplase rt = new TextReplase();
            rt.ShowDialog();

            Configures.Savetext();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //TODO: stop
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Channels c = new Channels();
            c.ShowDialog();
            Configures.SaveChatChannel();

            foreach (ChatController chat in Configures.Main.Chats)
            {
                chat.Update += NewMsg;
            }
        }

        private void NewMsg(ChatMessage msg)
        {
            UpdUserList();

            if (Configures.Main.NickNames.ContainsKey(msg.From))
            {
                if(Configures.Main.NickNames[msg.From].Ignore) return;
                }

                string RuleName = "[Default]";
            string SpeechName = Configures.ReadingOptions[0].SpeechName;
            if (Configures.ReadingOptions.Count != 1)
            {
                for (int i = 1; i < Configures.ReadingOptions.Count; i++)
                {
                    SpeechPref pref = Configures.ReadingOptions[i];
                    if (CheckRule(pref, msg))
                    {
                        RuleName = pref.RuleName;
                        SpeechName = pref.SpeechName;
                        break;
                    }
                }
            }

            string from = msg.From, to = "", body = msg.Text;

            if (Configures.Main.NickNames.ContainsKey(msg.From))
            {
                foreach (ChatSpeech speech in Configures.Main.NickNames[msg.From].Replace)
                {
                    if(speech.Type==RuleName)
                        if (speech.Name.Replace(" ", "") != "")
                        {
                            if(speech.Name!="")
                            from = speech.Name;
                            break;
                        }
                }
            }


            Tag t = SearchTag(msg.Text);
            if (t != null)
            {
                from = msg.From;
                if (Configures.Main.NickNames.ContainsKey(from))
                {
                    foreach (ChatSpeech speech in Configures.Main.NickNames[from].Replace)
                    {
                        if (speech.Type == RuleName)
                            if (speech.Name.Replace(" ", "") != "")
                            {
                                from = speech.Name;
                                break;
                            }
                    }
                }
                if (msg.To.Length == 1)
                {
                    to = msg.To[0];
                    if (Configures.Main.NickNames.ContainsKey(to))
                    {
                        foreach (ChatSpeech speech in Configures.Main.NickNames[to].Replace)
                        {
                            if (speech.Type == RuleName)
                                if (speech.Name.Replace(" ", "") != "")
                                {
                                    to = speech.Name;
                                    break;
                                }
                        }
                    }
                }

                foreach (ChatSpeech speech in Configures.TagList[0].TagText)
                {
                    if (speech.Type == RuleName)
                    {
                        if (speech.Name.Contains("%Ignore"))
                        {
                            body = "";
                            break;
                        }

                        if (msg.To.Length == 1)
                            from = speech.Name.Replace("%NameFrom", from);
                        else
                            from = speech.Name.Replace("%NameFrom", from).Replace("%NameTo", to);
                        break;
                    }
                }
            }
            else if (msg.To.Length == 0)
            {
                foreach (ChatSpeech speech in Configures.TagList[0].TagText)
                {
                    if (speech.Type == RuleName)
                    {
                        from = speech.Name.Replace("%NameFrom", from);
                        break;
                    }
                }
            }
            if (msg.To.Length == 1 && msg.ToStreammer)
            {
                foreach (ChatSpeech speech in Configures.TagList[3].TagText)
                    {
                        if (speech.Type == RuleName)
                        {
                            from = speech.Name.Replace("%NameFrom", from);
                        break;
                        }
                    }
            }
            else if (msg.To.Length == 1)
            {
                to = msg.To[0];
                if (Configures.Main.NickNames.ContainsKey(to))
                {
                    foreach (ChatSpeech speech in Configures.Main.NickNames[to].Replace)
                    {
                        if (speech.Type == RuleName)
                            if (speech.Name.Replace(" ", "") != "")
                            {
                                to = speech.Name;
                                break;
                            }
                    }
                }
                foreach (ChatSpeech speech in Configures.TagList[1].TagText)
                {
                    if (speech.Type == RuleName)
                    {
                        from = speech.Name.Replace("%NameFrom", from).Replace("%NameTo",to);
                        break;
                    }
                }
            }
            else if(msg.To.Length > 1)
            {
                foreach (ChatSpeech speech in Configures.TagList[2].TagText)
                {
                    if (speech.Type == RuleName)
                    {
                        from = speech.Name.Replace("%NameFrom", from);
                        break;
                    }
                }
            }


            if (body != "")
            {
                foreach (ChatText text in Configures.Main.TextReplace)
                {
                    foreach (ChatSpeech speech in text.Replace)
                    {
                        if (speech.Type == RuleName)
                        {
                            if (speech.Name != "")
                            {
                                body = body.Replace(text.Original, speech.Name);
                                break;
                            }

                        }
                    }
                }
            }

            foreach (SpeechController speech in Configures.Main.Speeches)
            {
                if (speech.ToString() == SpeechName)
                {
                    switch (speech.Info.Type)
                    {
                        case "SAPI":
                        {
                            SpeechController c = new SAPI(speech.Info.Name);
                            c.Prepare(from + body, (int) numericUpDown1.Value);
                                MessagesStack.Stack.Add(c);
                            break;
                        }
                        case "Google":
                        {
                                SpeechController c = new WebGoogle(speech.Info.Name);
                                c.Prepare(from + body, (int)numericUpDown1.Value);
                                MessagesStack.Stack.Add(c);
                                break;
                        }
                    }
                    break;
                }
            }

            if (!MessagesStack.ReadingNow||!MessagesStack.t.IsAlive)
            {
                MessagesStack.Read();
            }
        }

        Tag SearchTag(string msg)
        {
            string[] ss = msg.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
            if (ss.Length == 0) return null;

            if (Configures.TagList.Count == 4) return null;

            for (int i = 4; i < Configures.TagList.Count; i++)
            {
                Tag tag = Configures.TagList[i];
                if (tag.TagCode == ss[0]) return tag;
            }

            return null;
        }

        bool CheckRule(SpeechPref rule, ChatMessage msg)
        {
            switch (rule.RuleType)
            {
                case 0:
                {
                    foreach (char c in rule.RuleOps)
                    {
                        if (msg.Text.Contains(c.ToString())) return true;
                    }
                    break;
                }
                case 1:
                {
                    if (rule.RuleOps.Contains(msg.From)) return true;
                    break;
                }
            }
            return false;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            
        }
        
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e) {}
        
        private void logToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Log l = new Log();
            l.ShowDialog();
        }

        private void синтезаторыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VoiceSetup vs = new VoiceSetup();
            vs.ShowDialog();
            Configures.SaveChatSpeech();
        }

        private void основныеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Configs cfgs = new Configs();
            cfgs.ShowDialog();
            Configures.SaveReaderConfig();
        }

        private void тегиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tags t = new Tags();
            t.ShowDialog();
            Configures.SaveTags();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Configures.Init();
            button5_Click(null, null);
        }

        private void правилаСинтезаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RuleMenu rm = new RuleMenu();
            rm.ShowDialog();
            Configures.SaveReadingOptions();
        }
        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}