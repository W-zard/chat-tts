﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BotSpeaker.Rules;

namespace BotSpeaker.Rule
{
    public partial class RuleMenu : Form
    {
        public RuleMenu()
        {
            InitializeComponent();
        }

        public SpeechPref Rule;

        void UpdateForm()
        {
            comboBox1.Items.Clear();
            comboBox2.Items.Clear();
            textBox1.Text = "";
            richTextBox1.Text = "";

            if (Rule != null)
            {
                textBox1.ReadOnly = listBox1.SelectedIndex <= 0;
                textBox1.Text = Rule.RuleName;

                comboBox2.Items.Clear();

                if (Rule.RuleType == -1)
                {
                    comboBox2.Items.Add("[DEFAULT]");
                    comboBox2.SelectedIndex = 0;
                }
                else
                {
                    comboBox2.Items.Add("По символу");
                    comboBox2.Items.Add("По нику");
                    comboBox2.SelectedIndex = Rule.RuleType;
                }

                textBox1.ReadOnly = listBox1.SelectedIndex <= 0;
                richTextBox1.Text = Rule.RuleOps;

                
                int z = -1;
                for (int i = 0; i < Configures.Main.Speeches.Count; i++)
                {
                    string s = Configures.Main.Speeches[i].GetName();
                    comboBox1.Items.Add(s);
                    if (s == Rule.SpeechName) z = i;
                }
                comboBox1.SelectedIndex = z;
            }
        }

        void UpdateList(int count)
        {
            int z = listBox1.SelectedIndex;
            UpdateList();
            if(listBox1.Items.Count>=z+count) count = 0;
            listBox1.SelectedIndex = z + count;
        }

        void UpdateList()
        {
            listBox1.Items.Clear();
            foreach (SpeechPref pref in Configures.ReadingOptions)
            {
                listBox1.Items.Add(pref.RuleName);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SpeechPref newr = new SpeechPref();
            Rule = newr;
            Configures.ReadingOptions.Add(newr);
            
            UpdateList();

            listBox1.SelectedIndex = listBox1.Items.Count - 1;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex==-1) return;
            if (fromtextbox)
            {
                fromtextbox = false;
                return;
            }
            savingmode = false;
            Rule = Configures.ReadingOptions[listBox1.SelectedIndex];
            UpdateForm();
            savingmode = true;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if(listBox1.SelectedIndex < 1) return;

            SpeechPref tmp = Configures.ReadingOptions[listBox1.SelectedIndex - 1];

            Configures.ReadingOptions[listBox1.SelectedIndex - 1] = Configures.ReadingOptions[listBox1.SelectedIndex];
            Configures.ReadingOptions[listBox1.SelectedIndex] = tmp;

            UpdateList(-1);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if(listBox1.SelectedIndex<=0) return;
            if (listBox1.Items.Count - 1 == listBox1.SelectedIndex) return;

            SpeechPref tmp = Configures.ReadingOptions[listBox1.SelectedIndex + 1];

            Configures.ReadingOptions[listBox1.SelectedIndex + 1] = Configures.ReadingOptions[listBox1.SelectedIndex];
            Configures.ReadingOptions[listBox1.SelectedIndex] = tmp;

            UpdateList(+1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(listBox1.SelectedIndex<1) return;
            Configures.ReadingOptions.Remove(Configures.ReadingOptions[listBox1.SelectedIndex]);
            UpdateList();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if(Rule==null) return;
            if(listBox1.SelectedIndex<1)return;

            switch (comboBox2.SelectedIndex)
            {
                case 0:
                {
                    AddChar ac = new AddChar();
                    if (ac.ShowDialog() == DialogResult.OK)
                    {
                        foreach (char c in ac.Result)
                        {
                            if (!Rule.RuleOps.Contains(c)) Rule.RuleOps += c;
                        }
                        UpdateForm();
                    }
                    break;
                }
                case 1:
                {
                    AddUserName ac = new AddUserName();
                    if (ac.ShowDialog() == DialogResult.OK)
                    {
                        richTextBox1.Text += ac.Result+"\n";
                    }
                    break;
                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex <= 0) return;
            if (savingmode)
            {
                Rule.RuleName = textBox1.Text;
                fromtextbox = true;
                listBox1.Items[listBox1.SelectedIndex] = textBox1.Text;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Rule.SpeechName = comboBox1.SelectedItem.ToString();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex <= 0) return;
            Rule.RuleType = comboBox2.SelectedIndex;
        }

        private void RuleMenu_Load(object sender, EventArgs e)
        {
            UpdateList();
        }

        private bool savingmode = false;
        private bool fromtextbox = false;

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            if(listBox1.SelectedIndex<=0) return;
            if(savingmode) Rule.RuleOps = richTextBox1.Text;
        }
    }
}
