﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BotSpeaker.Rules
{
    public partial class AddUserName : Form
    {
        public AddUserName()
        {
            InitializeComponent();

            foreach (string s in Configures.Main.ActiveNames)
            {
                comboBox1.Items.Add(s);
            }
        }

        public string Result;

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Result = comboBox1.Text;

            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
