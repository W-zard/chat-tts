﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BotSpeaker.Speech
{
    public class ProcessCmd : SpeechController
    {
        public ProcessCmd(string str)
        {
            Command = str;
        }

        public string Command = "";

        public override event UpdateEvent Update;
        public override void Prepare(string str, int rate)
        {
            
        }

        public override event EndEvent End;

        public override void Say()
        {
            Process p = new Process();
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = false;

            //p.StartInfo.FileName = Command.Re;
            //p.StartInfo.Arguments = "-loglevel panic -i \"" + filepath + ".mp3" + "\" -f wav -acodec pcm_s16le \"" + filepath + ".wav" + "\" -y";
            p.Start();
        }
        
        public override string GetName()
        {
            throw new NotImplementedException();
        }

        public override void Abort()
        {
            throw new NotImplementedException();
        }
    }
}
