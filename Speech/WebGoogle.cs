﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BotSpeaker.Speech
{
    public class WebGoogle : SpeechController
    {
        public WebGoogle(string language)
        {
            Info = new ChatSpeech();
            Info.Type = "Google";
            Info.Name = language;
        }

        WMPLib.WindowsMediaPlayer wplayer = new WMPLib.WindowsMediaPlayer();

        private string f;

        public override event UpdateEvent Update;
        public override void Prepare(string str, int rate)
        {
            Thread tt = new Thread(() =>
                                   {
                                       float z = (rate + 10)/20;
                                       if (
                                           !Directory.Exists(
                                                             Environment.GetFolderPath(
                                                                                       Environment.SpecialFolder.
                                                                                                   ApplicationData) +
                                                             "\\speechTemp"))
                                           Directory.CreateDirectory(
                                                                     Environment.GetFolderPath(
                                                                                               Environment.SpecialFolder.
                                                                                                           ApplicationData) +
                                                                     "\\speechTemp");

                                       f = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
                                           "\\speechTemp\\" + DateTime.Now.Ticks + ".mp3";

                                       WebClient wc = new WebClient();
                                       bool d = false;

                                       while (d)
                                       {
                                           try {
                                               wc.DownloadFile(
                                                      new Uri(
                                                          "https://translate.google.com.ua/translate_tts?ie=UTF-8&q=" +
                                                          str + "&tl=" + Info.Name + "&client=t&ttsspeed=" +
                                                          z.ToString("0.00")).AbsoluteUri, f);
                                               d = true;
                                           }
                                           catch (Exception)
                                           {
                                               continue;
                                           }
                                       }

                                       
                                       IsReady = true;
                                   });
            tt.Start();
        }

        public override event EndEvent End;

        private Thread t;

        public override void Say()
        {
            if(Sayd)return;

            t = new Thread(() =>
            {
                wplayer.PlayStateChange += delegate(int state)
                                           {
                                               if ((WMPLib.WMPPlayState) state == WMPLib.WMPPlayState.wmppsStopped)
                                               {
                                                   File.Delete(f);
                                                   Sayd = true;
                                                   if (End != null) End();
                                               }
                                           };
                
                wplayer.URL = f;
                wplayer.controls.pause();


            });
            t.Start();
        }

        public override string GetName()
        {
            return Info.Type + ": " + Info.Name;
        }

        public override void Abort()
        {
            if (t.IsAlive)
            {
                t.Abort();
                if (End != null) End();
            }
        }
    }
}
