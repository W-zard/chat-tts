﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SpeechLib;

namespace BotSpeaker
{
    public class SAPI : SpeechController
    {

        public static string GGPasswd = "7895123";
        public SAPI(string name)
        {
            Info = new ChatSpeech();
            Info.Type = "SAPI";
            Info.Name = name;
            foreach (SpObjectToken token in voice.GetVoices("", ""))
            {
                if(name==token.GetDescription(0))
                voice.Voice = token;
            }
        }

        public int Rate;

        public override event UpdateEvent Update;
        public override void Prepare(string str, int rate)
        {
            TextToRead = str;
            Rate = rate;
            IsReady = true;
            if (Sayd) return;
            t = new Thread(() =>
            {
                voice.Rate = Rate;
                voice.Speak(TextToRead, SpeechVoiceSpeakFlags.SVSFNLPSpeakPunc);
                Sayd = true;
                if (End != null) End();
            });
            t.Start();

        }

        public override event EndEvent End;

        public static SpVoice voice = new SpVoice();

        private Thread t;

        public override void Say()
        {
            
            
        }
        
        public override string GetName()
        {
            return Info.Type + ": " + Info.Name;
        }

        public override void Abort()
        {
            if (t.IsAlive)
            {
                t.Abort();
                if (End != null) End();
            }
        }
    }
}
